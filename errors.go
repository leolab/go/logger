package logger

import "gitlab.com/leolab/go/errs"

const (
	ErrConfigError   errs.ErrCode = "ErrConfigError"
	ErrFileError     errs.ErrCode = "ErrFileError"
	ErrDSError       errs.ErrCode = "ErrDSError"
	ErrDSUnsupported errs.ErrCode = "ErrDSUnsupported"
)
