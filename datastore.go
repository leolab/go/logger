package logger

import (
	"strings"

	"gitlab.com/leolab/go/errs"
	"gitlab.com/leolab/go/file"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func __getDS(dsn string) (ds *gorm.DB, err *errs.Err) {
	cfg := &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	}
	ds = nil
	var e error
	dsa := strings.Split(dsn, ">")
	switch dsa[0] {
	case "sqlite":
		ds, e = gorm.Open(sqlite.Open(file.RealPath(dsa[1])), cfg)
		if e != nil {
			return nil, errs.RaiseError(ErrDSError, e.Error())
		}
	case "mysql":
		ds, e = gorm.Open(mysql.Open(dsa[1]), cfg)
		if e != nil {
			return nil, errs.RaiseError(ErrDSError, e.Error())
		}
	case "postgres":
		ds, e = gorm.Open(postgres.New(postgres.Config{
			DSN:                  dsa[1],
			PreferSimpleProtocol: true,
		}), cfg)
		if e != nil {
			return nil, errs.RaiseError(ErrDSUnsupported, "Postgres data store")
		}
	default:
		return nil, errs.RaiseError(ErrDSUnsupported, "Unsupported data driver: "+dsa[0])
	}
	//fmt.Println("DSN: " + dsn)
	return ds, nil
}
