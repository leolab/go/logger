# Пакет логгера
> Version: 0.2.0

## Fetures
- Логгер, кроме фунции (с файлом и строкой) из которой вызван, сохраняет идентификатор потока
- Логгер параллельно с выводом записывает сообщения в файл и в базу данных
- Гибкая настройка выводимых и сохраняемых типов сообщений
- Работа с файлом конфирурации "из коробки"

## Featured
- Отправка сообщений в лог-сервер

## Использование
```go
import (
    "gitlab.com/leolab/go/logger"
    "gitlab.com/leolab/go/AppConf"
)

var log Logger

func main(){
    cfg,err:=appconf.NewAppConf()
    if err!=nil{
        panic(err.Error())
    }
    log,err = logger NewLogger(cfg)
    if err!=nil{
        panic(err.Error())
    }

}

```

## Теги замены
Теги применяются в шаблонах вывода лога в консоль и записи в файл

- `%file%`
- `%line%`
- `%func%`
- `%type%` - Уровень лога
- `%msg%`
- `%goID%` - идентификатор рутины
- `%procID%` - идентификатор процесса (системный)

## Параметры
- `--logger.file`
- `--logger.dsn`
- `--logger.levels` = "" - общие параметры, либо:
    - `--logger.levels.verbose` = "debug,notify,warning,error,request,event"
    - `--logger.levels.file` = "info,warning,error,request,event"
    - `--logger.level.store` = "debug,info,notify,warning,error,request,event"
- `--logger.dbg` = "false"
- `--logger.silent` = "true"
- `--logger.tpl.file` = "%date% %time%.%time.ns%  %procID%:%goID% [%type%] %msg% from %func% (%file%:%line%)"
- `--logger.tpl.cons` = "%date% %time%.%time.ns%  %procID%:%goID% [%type%] %msg% from %func% (%file%:%line%)"

## Уровни логгера
- `debug`
- `info`
- `notify`
- `warning`
- `error`
- `request`
- `event`
