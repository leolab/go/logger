package logger

import (
	"fmt"
	"testing"

	"gitlab.com/leolab/go/appconf"
)

//var cfg *appconf.AppConf
var log *Logger

func TestNewLogger(t *testing.T) {
	cfg, err := appconf.NewAppConf("test/logger.json")
	if err != nil {
		t.Fatal(err)
	}
	log, err = NewLogger(cfg)
	if err != nil {
		t.Fatal(err)
	}

}
func TestLogWrite(t *testing.T) {
	log.Debug("Debug log record")
	log.Info("Information record")
	log.Notify("Notification record")
	log.Warn("Warning record")
	log.Error("Error record")
	log.Request("Request record")
	log.Event("Event record")
}

func TestLogRead(t *testing.T) {
	lr := log.GetRecs(0, 20)
	for _, r := range lr {
		fmt.Println(r)
	}
	lr = log.GetRecs(0, 20, &LoggerRec{Lvl: lvlNames[LogLevelInfo]})
	for _, r := range lr {
		fmt.Println(r)
	}
}

func TestConcurences(t *testing.T) {
	/*
		log.Info("Test concurence write")
		wg := &sync.WaitGroup{}
		for i := 0; i < 5; i++ {
			wg.Add(1)
			go func(i int, wg *sync.WaitGroup) {
				defer wg.Done()
				var max big.Int
				max.SetUint64(50)
				cnt, _ := rand.Int(rand.Reader, &max)

				for rc := 0; rc < int(cnt.Int64()); rc++ {
					var max big.Int
					max.SetUint64(5)
					rs, _ := rand.Int(rand.Reader, &max)
					time.Sleep(time.Duration(rs.Int64()) * time.Microsecond)
					LogTest.Info("Routine [" + strconv.Itoa(i) + "/" + strconv.Itoa(int(cnt.Int64())) + "]:" + strconv.Itoa(rc) + "/" + strconv.Itoa(int(cnt.Int64())))
				}
			}(i, wg)
		}
		wg.Wait()
		LogTest.Info("Done")
	*/
}
