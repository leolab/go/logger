module gitlab.com/leolab/go/logger

go 1.16

require (
	gitlab.com/leolab/go/appconf v1.1.5
	gitlab.com/leolab/go/errs v0.0.3
	gitlab.com/leolab/go/file v1.0.2
	gitlab.com/leolab/go/replacer v1.0.1
	gorm.io/driver/mysql v1.1.2
	gorm.io/driver/postgres v1.1.2
	gorm.io/driver/sqlite v1.1.6
	gorm.io/gorm v1.21.16
)
