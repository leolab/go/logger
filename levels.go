package logger

import (
	"strings"

	"gitlab.com/leolab/go/errs"
)

type LogLevels struct {
	levels map[LogLevel]bool
}

func newLogLevels(a ...string) (lvl *LogLevels, err *errs.Err) {
	lvl = &LogLevels{
		levels: make(map[LogLevel]bool),
	}
	for _, lv := range a {
		va := strings.Split(strings.Join(strings.Split(lv, " "), ","), ",")
		for _, lva := range va {
			if v, ok := logLevels[lva]; !ok {
				return nil, errs.RaiseError(ErrConfigError, "Unknown log level: "+lv)
			} else {
				lvl.Add(v)
			}
		}
	}
	return lvl, nil
}

type LogLevel int

const (
	LogLevelDebug   LogLevel = iota
	LogLevelInfo             // LogLevel = 8
	LogLevelNotify           // LogLevel = 4
	LogLevelWarning          // LogLevel = 2
	LogLevelError            // LogLevel = 1
	//Special levels
	LogLevelRequest // = 20
	LogLevelEvent   // = 40
)

var logLevels map[string]LogLevel = map[string]LogLevel{
	"debug":   LogLevelDebug,
	"info":    LogLevelInfo,
	"notify":  LogLevelNotify,
	"warning": LogLevelWarning,
	"error":   LogLevelError,
	"request": LogLevelRequest,
	"event":   LogLevelEvent,
}

var lvlNames map[LogLevel]string = map[LogLevel]string{
	LogLevelDebug:   "debug",
	LogLevelInfo:    "info",
	LogLevelNotify:  "notify",
	LogLevelWarning: "warning",
	LogLevelError:   "error",
	LogLevelRequest: "request",
	LogLevelEvent:   "event",
}

//Установить (переустановить) уровень логгирования
func (l *LogLevels) Set(lvl ...LogLevel) {
	l.levels = make(map[LogLevel]bool)
	for _, lv := range lvl {
		l.levels[lv] = true
	}
}

//Добавить уровень логгирования
func (l *LogLevels) Add(lvl LogLevel) {
	l.levels[lvl] = true
}

//Удалить уровень логгирования
func (l *LogLevels) Del(lvl LogLevel) {
	//if _, ok := l.levels[lvl]; ok {
	delete(l.levels, lvl)
	//}
}

//Установлен ли уровень
func (l *LogLevels) IsSet(lvl LogLevel) bool {
	_, ok := l.levels[lvl]
	return ok
}
