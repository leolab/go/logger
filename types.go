package logger

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gitlab.com/leolab/go/errs"
)

type LoggerRec struct {
	Time   time.Time `gorm:"index"`
	Lvl    string    `gorm:"index"`
	Msg    string
	File   string
	Line   int
	Func   string
	GoID   int
	ProcID int `gorm:"index"`
	//Src    __srcRec //JSON-string(__src)
	Data string //JSON-data
}

type __srcRec struct {
	ProcID int
	GoID   int
	File   string
	Line   int
	Func   string
}

func __goId() int {
	var buf [64]byte
	n := runtime.Stack(buf[:], false)
	r, e := strconv.ParseInt(strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0], 10, 64)
	if e != nil {
		err := errs.RaiseError(errs.ErrError, "Error getting goID: "+e.Error())
		fmt.Println("Error: ", err)
	}
	return int(r)
}

func __getSrc(lvl int) (s __srcRec) {
	pc, file, line, ok := runtime.Caller(lvl)
	if !ok {
		err := errs.RaiseError(errs.ErrError, "Error getting runtime")
		fmt.Println("Error: ", err)
		return __srcRec{File: "Undefined", Line: 0, Func: "Error"}
	}
	f := runtime.CallersFrames([]uintptr{pc})
	ff, _ := f.Next()
	return __srcRec{
		ProcID: os.Getpid(),
		GoID:   __goId(),
		File:   file,
		Line:   line,
		Func:   ff.Function,
	}
}
