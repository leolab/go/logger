package logger

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"

	"gitlab.com/leolab/go/appconf"
	"gitlab.com/leolab/go/errs"
	"gitlab.com/leolab/go/file"
	"gitlab.com/leolab/go/replacer"
	"gorm.io/gorm"
)

type Logger struct {
	Dbg        bool       //=true - к выводу добавляется файл и строка вызова
	Silent     bool       //=true - не производится вывод в консоль
	FileName   string     // имя файла, возможно использование функций replacer
	DSN        string     // имя базы данных (DSN)
	VerbLevel  *LogLevels // Уровень сообщений для вывода в консоль
	FileLevel  *LogLevels // Уровень сообщений для записи в файл
	StoreLevel *LogLevels // Уровень сообщений для записи в БД
	//= Templates >
	FileTemplate string
	ConsTemplate string
	//< Templates =
	//= Internals >
	fName string // Текущее имя файла
	//dsn   string   // Текущее имя базы данных
	ds *gorm.DB // Подключение к БД
	fs *os.File // Текущий открытый файл
	//< Internals =
	//= Locks >
	lock   *sync.Mutex
	fsLock *sync.Mutex
	dsLock *sync.Mutex
	//< Locks =
	r *replacer.Replacer
}

func NewLogger(cfg *appconf.AppConf) (log *Logger, err *errs.Err) {
	//== Config >>
	if cfg == nil {
		return nil, errs.RaiseError(ErrConfigError, "Configuration is nil")
	}
	log = &Logger{
		Dbg:      false,
		Silent:   false,
		FileName: cfg.Value("logger.file"),
		DSN:      cfg.Value("logger.dsn"),
	}
	if cfg.Exists("logger.levels") {
		lvls, err := newLogLevels(cfg.Value("logger.levels"))
		if err != nil {
			return nil, errs.UpError(err)
		}
		log.VerbLevel = lvls
		log.FileLevel = lvls
		log.StoreLevel = lvls
	} else {
		if cfg.Exists("logger.levels.verbose") {
			log.VerbLevel, err = newLogLevels(cfg.Value("logger.levels.verbose"))
			if err != nil {
				return nil, errs.UpError(err)
			}
		} else {
			log.VerbLevel, err = newLogLevels("debug,notify,warning,error,request,event")
			if err != nil {
				return nil, errs.UpError(err)
			}
		}
		if cfg.Exists("logger.levels.file") {
			log.FileLevel, err = newLogLevels(cfg.Value("logger.levels.file"))
			if err != nil {
				return nil, errs.UpError(err)
			}
		} else {
			log.FileLevel, err = newLogLevels("info,warning,error,request,event")
			if err != nil {
				return nil, errs.UpError(err)
			}
		}
		if cfg.Exists("logger.levels.store") {
			log.StoreLevel, err = newLogLevels(cfg.Value("logger.levels.store"))
			if err != nil {
				return nil, errs.UpError(err)
			}
		} else {
			log.StoreLevel, err = newLogLevels("debug,info,notify,warning,error,request,event")
			if err != nil {
				return nil, errs.UpError(err)
			}
		}
	}
	if cfg.Exists("logger.dbg") {
		log.Dbg, err = cfg.Bool("logger.dbg")
		if err != nil {
			return nil, errs.UpError(err)
		}
	}
	if cfg.Exists("logger.silent") {
		log.Silent, err = cfg.Bool("logger.silent")
		if err != nil {
			return nil, errs.UpError(err)
		}
	}
	//= Templates >
	if cfg.Exists("logger.tpl.file") {
		log.FileTemplate = cfg.Value("logger.tpl.file")
	} else {
		log.FileTemplate = "%date% %time%.%time.ns%  %procID%:%goID% [%type%] %msg% from %func% (%file%:%line%)"
	}
	if cfg.Exists("logger.tpl.cons") {
		log.ConsTemplate = cfg.Value("logger.tpl.cons")
	} else {
		log.ConsTemplate = "%date% %time%.%time.ns%  %procID%:%goID% [%type%] %msg% from %func% (%file%:%line%)"
	}
	//< Templates =
	//<< Config ==
	//== Init internals >>
	log.lock = &sync.Mutex{}
	log.dsLock = &sync.Mutex{}
	log.fsLock = &sync.Mutex{}
	log.r = replacer.NewReplacer()

	if err := log._checkFile(); err != nil {
		return nil, errs.UpError(err)
	}
	if err := log._checkDS(); err != nil {
		return nil, errs.UpError(err)
	}
	//<< Init internals ==
	return log, nil
}

//== Main methods >>

func (l *Logger) Debug(msg string, data ...interface{}) {
	l.log(LogLevelDebug, msg, data)
}

func (l *Logger) Info(msg string, data ...interface{}) {
	l.log(LogLevelInfo, msg, data)
}

func (l *Logger) Notify(msg string, data ...interface{}) {
	l.log(LogLevelNotify, msg, data)
}

func (l *Logger) Warn(msg string, data ...interface{}) {
	l.log(LogLevelWarning, msg, data)
}

func (l *Logger) Error(msg string, data ...interface{}) {
	l.log(LogLevelWarning, msg, data)
}

func (l *Logger) Request(msg string, data ...interface{}) {
	l.log(LogLevelRequest, msg, data)
}

func (l *Logger) Event(msg string, data ...interface{}) {
	l.log(LogLevelEvent, msg, data)
}

//<< Main methods ==

//== DS Methods >>

func (l *Logger) GetRecs(from, limit int, f ...*LoggerRec) (lr []*LoggerRec) {
	if limit < 0 {
		limit = 50
	}
	if len(f) > 0 {
		e := l.ds.Order("time desc").Where(&f[0]).Offset(from).Limit(limit).Find(&lr).Error
		if e != nil {
			fmt.Fprintln(os.Stderr, e)
		}
	} else {
		e := l.ds.Order("time desc").Offset(from).Limit(limit).Find(&lr).Error
		if e != nil {
			fmt.Fprintln(os.Stderr, e)
		}
	}
	return
}

//<< DS Methods ==

func (l *Logger) log(lvl LogLevel, msg string, data ...interface{}) {
	_src := __getSrc(3)
	lr := &LoggerRec{
		Time:   time.Now(),
		Lvl:    lvlNames[lvl],
		Msg:    msg,
		File:   _src.File,
		Line:   _src.Line,
		Func:   _src.Func,
		GoID:   _src.GoID,
		ProcID: _src.ProcID,
		Data:   l._getJSON(data),
	}

	lrr := replacer.NewReplacer()
	lrr.Add("file", lr.File)
	lrr.Add("line", strconv.Itoa(lr.Line))
	lrr.Add("func", lr.Func)
	lrr.Add("type", lr.Lvl)
	lrr.Add("msg", lr.Msg)
	lrr.Add("goID", strconv.Itoa(lr.GoID))
	lrr.Add("procID", strconv.Itoa(lr.ProcID))
	l._print(lrr)
	l._write(lrr)
	l._store(lr)
}

//Вывод в консоль
func (l *Logger) _print(r *replacer.Replacer) {
	l.lock.Lock()
	defer l.lock.Unlock()
	_, e := fmt.Println(r.Replace(l.ConsTemplate))
	if e != nil {
		fmt.Fprintln(os.Stderr, "Logger: ", e)
	}
}

//Запись в файл
func (l *Logger) _write(r *replacer.Replacer) {
	l.fsLock.Lock()
	defer l.fsLock.Unlock()
	_, e := l.fs.WriteString(r.Replace(l.FileTemplate))
	if e != nil {
		fmt.Fprintln(os.Stderr, "Logger: ", e)
	}
}

//Сохранение в БД
func (l *Logger) _store(lr *LoggerRec) {
	l.dsLock.Lock()
	defer l.dsLock.Unlock()
	e := l.ds.Create(*lr).Error
	if e != nil {
		fmt.Fprintln(os.Stderr, "Logger: ", e)
	}
}

func (l *Logger) _getJSON(data ...interface{}) string {
	d, e := json.Marshal(data)
	if e != nil {
		fmt.Fprintln(os.Stderr, "Logger: ", e, __getSrc(3))
	}
	return string(d)
}

func (l *Logger) _checkFile() (err *errs.Err) {
	l.fsLock.Lock()
	defer l.fsLock.Unlock()
	if l.FileName == "" {
		if l.fs != nil {
			l.fs.Close()
			l.fs = nil
		}
		return nil
	}
	var e error
	fn, ea := l.r.ReplaceCE(l.FileName)
	if ea != nil {
		return errs.RaiseError(errs.ErrError, "Error parse file name", map[string]interface{}{"Errors": ea})
	}
	fn = file.RealPath(fn)
	if l.fName != fn {
		if l.fName != "" {
			if l.fs != nil {
				if e := l.fs.Close(); e != nil {
					errs.RaiseError(ErrFileError, "Error close file ("+l.fName+"): "+e.Error())
				}
			}
		}
		l.fName = fn
		l.fs, e = os.OpenFile(l.fName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0664)
		if e != nil {
			return errs.RaiseError(ErrFileError, "Error open file ("+l.fName+"): "+e.Error())
		}
	}
	return nil
}

func (l *Logger) _checkDS() (err *errs.Err) {
	l.dsLock.Lock()
	defer l.dsLock.Unlock()
	if l.DSN == "" {
		if l.ds != nil {
			l.ds = nil
		}
		return nil
	}
	if l.ds != nil {
		l.ds = nil
	}
	l.ds, err = __getDS(l.DSN)
	if err != nil {
		return errs.UpError(err)
	}
	e := l.ds.AutoMigrate(&LoggerRec{})
	if e != nil {
		return errs.RaiseError(ErrDSError, e.Error())
	}
	return nil
}
